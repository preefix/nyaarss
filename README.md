
## NyaaRSS : Yet another RSS for weebs

Author: Noah <noah@moron.agency>

Based on [YaRSS2 by Bro/bendikro](https://bitbucket.org/bendikro/deluge-yarss-plugin/downloads/)

License: GPLv3

## Building the plugin ##

```
#!bash

$ python setup.py bdist_egg
```


## Running the tests ##
The directory containing yarss2 must be on the PYTHONPATH

e.g.

```
#!bash

yarss2$ export PYTHONPATH=$PYTHONPATH:$PWD/..
```


Run the tests with:

```
#!bash

yarss2$ trial tests
```
